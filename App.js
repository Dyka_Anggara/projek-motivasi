import React, { Component } from 'react';
import {
  Text,
  TextInput,
  View,
  StyleSheet,
  FlatList,
  StatusBar,
} from 'react-native';

let motivasi = [
  { genre: 'bakat 1', word: 'Bakat terbentuk dalam gelombang kesunyian, watak terbentuk dalam riak besar kehidupan.' },
  { genre: 'bakat 2', word: 'Bakat adalah sesuatu yang kau tumbuhkan. Insting adalah sesuatu yang harus kita latih.' },
  { genre: 'bakat 3', word: 'Saya tidak memiliki bakat khusus. Saya hanya ingin tahu. - Albert Einstein' },
  { genre: 'bakat 4', word: 'Bahagia itu bukan bakat, tapi akibat-akibat dari kesyukuran, buah dari kesabaran.' },
  { genre: 'bakat 5', word: 'Hebat itu bukan karena bakat. Melainkan hasil dari hempasan tekad dan banyak kerja keras.' },
  { genre: 'bakat 6', word: 'Jika seseorang punya bakat dan hanya menggunakan setengahnya, dia setengah gagal.' },
  { genre: 'bakat 7', word: 'Semua manusia terlahir dengan bakat masing-masing.' },
  { genre: 'bakat 8', word: 'Ada satu hal yang bisa mengalahkan bakat, yaitu keinginan dan semangat yang kuat.' },
  { genre: 'bakat 9', word: 'Bakat yang besar tidak akan ada artinya tanpa keberanian dan kerja keras.' },
  { genre: 'bakat 10', word: 'Keberhasilan ditentukan oleh 99% usaha dan 1% bakat.' },
  { genre: 'cinta 1', word: 'Cinta itu layaknya angin, aku tidak bisa melihatnya tetapi aku bisa merasakannya. - Nicholas Sparck' },
  { genre: 'cinta 2', word: 'Akhir yang benar-benar bahagia datang setelah sebuah kisah dengan banyak suka dan duka.' },
  { genre: 'cinta 3', word: 'Pasangan paling bahagia di dunia ini tidak pernah memiliki sifat yang sama. Mereka hanya saling memahami dengan baik tentang perbedaan yang mereka miliki.' },
  { genre: 'cinta 4', word: 'Cinta bukanlah bertahan seberapa lama. Tetapi seberapa jelas dan ke arah mana. - Emha Ainun Nadjib' },
  { genre: 'cinta 5', word: 'Allah menciptakan senja untuk mengingatkanku untuk pulang pada cinta yang kukenang.' },
  { genre: 'cinta 6', word: 'Tak perlu yang mewah asal bisa membuat bahagia saat sepi melanda cukup sederhana saja melihat canda tawamu.' },
  { genre: 'cinta 7', word: 'Cinta mungkin hadir karena takdir tapi tak ada salahnya kita saling memperjuangkan.' },
  { genre: 'cinta 8', word: 'Carilah orang-orang yang tidak mudah bilang suka, tapi saat bilang, dia langsung bawa satu rombongan keluarga.' },
  { genre: 'cinta 9', word: 'Aku menginginkanmu seutuhnya, selamanya, kamu dan aku, setiap hari. - Nicholas Sparks' },
  { genre: 'cinta 10', word: 'Mencintai diri sendiri adalah awal dari romansa seumur hidup. - Oscar Wilde' },
  { genre: 'islami 1', word: 'Maka sesungguhnya bersama kesulitan itu ada kemudahan. - QS Al Insyirah 5' },
  { genre: 'islami 2', word: 'Berpikirlah positif, tidak peduli seberapa keras kehidupanmu. - Ali bin Abi Thalib' },
  { genre: 'islami 3', word: 'Kita adalah makhluk yang suka menyalahkan dari luar, tidak menyadari bahwa masalah biasanya dari dalam. - Abu Hamid Al Ghazali' },
  { genre: 'islami 4', word: 'Dunia ini ibarat bayangan. Kalau kau berusaha menangkapnya, ia akan lari. Tapi kalau kau membelakanginya, ia tak punya pilihan selain mengikutimu. - Ibnu Qayyim Al Jauziyyah' },
  { genre: 'islami 5', word: 'Balas dendam terbaik adalah menjadikan dirimu lebih baik. - Ali bin Abi Thalib' },
  { genre: 'islami 6', word: 'Jangan berduka, apa pun yang hilang darimu akan kembali lagi dalam wujud lain. - Jalaludin Rumi' },
  { genre: 'islami 7', word: 'Janganlah kamu berduka cita, sesungguhnya Allah selalu bersama kita.- QS At Taubah 40' },
  { genre: 'islami 8', word: 'Allah menciptakan senja untuk mengingatkanku untuk pulang pada cinta yang kukenang.' },
  { genre: 'islami 9', word: 'Orang yang terkaya adalah orang yang menerima pembagian (taqdir) dari Allah dengan senang hati. - Ali bin Husein' },
  { genre: 'islami 10', word: 'Jangan menjelaskan tentang dirimu kepada siapa pun, karena yang menyukaimu tidak butuh itu. Dan yang membencimu tidak percaya itu. - Ali bin Abi Thalib' },
  { genre: 'kehidupan 1', word: 'Setiap orang berpikir untuk mengubah dunia, tapi tidak ada yang berpikir untuk mengubah dirinya sendiri. - Leo Tolstoy' },
  { genre: 'kehidupan 2', word: 'Pejuang yang sukses adalah orang biasa, dengan fokus seperti laser. - Bruce Lee' },
  { genre: 'kehidupan 3', word: 'Hal terpenting dalam hidup adalah belajar bagaimana memberikan cinta, dan membiarkannya masuk. - Morrie Schwartz' },
  { genre: 'kehidupan 4', word: 'Jika kamu hidup, kamu adalah orang yang kreatif. - Elizabeth Gilbert' },
  { genre: 'kehidupan 5', word: 'Tujuan hidup kita adalah menjadi bahagia. - Dalai Lama' },
  { genre: 'kehidupan 6', word: 'Semua kehidupan adalah eksperimen. Makin banyak eksperimen yang Anda, buat makin baik. - Ralph Waldo Emerson' },
  { genre: 'kehidupan 7', word: 'Beri nilai dari usahanya, jangan dari hasilnya. Baru kita bisa menilai kehidupan. - Albert Einstein' },
  { genre: 'kehidupan 8', word: 'Saya telah gagal berulang kali dalam hidup saya. Dan itulah mengapa saya berhasil. - Michael Jordan' },
  { genre: 'kehidupan 9', word: 'Tidak ada kata terlambat untuk mulai menciptakan kehidupan yang kamu inginkan. - Dawn Clark' },
  { genre: 'kehidupan 10', word: 'Selama Anda hidup, teruslah belajar bagaimana hidup. - Seneca' },
  { genre: 'Pendidikan 1', word: 'Tujuan utama pendidikan bukanlah pengetahuan, tetapi tindakan.' },
  { genre: 'Pendidikan 2', word: 'Pendidikan adalah seni untuk membuat manusia makin berkarakter.' },
  { genre: 'Pendidikan 3', word: 'Menjadi cerdas dan berkarakter adalah tujuan utama sebuah pendidikan.' },
  { genre: 'Pendidikan 4', word: 'Pengetahuan adalah kekuatan. Informasi membebaskan. Pendidikan adalah premis kemajuan, di setiap masyarakat, di setiap keluarga. - Kofi Annan' },
  { genre: 'Pendidikan 5', word: 'Akar pendidikan itu pahit, tapi buahnya manis. - Aristoteles' },
  { genre: 'Pendidikan 6', word: 'Pendidikan adalah senjata paling ampuh yang dapat kamu gunakan untuk mengubah dunia. - Nelson Mandela' },
  { genre: 'Pendidikan 7', word: 'Pendidikan adalah kemampuan untuk mendengarkan hampir semua hal tanpa kehilangan kesabaran atau kepercayaan diri. - Robert Frost' },
  { genre: 'Pendidikan 8', word: 'Dasar dari setiap negara adalah pendidikan para pemuda. - Diogenes' },
  { genre: 'Pendidikan 9', word: 'Tanpa pengetahuan, tindakan tidak berguna dan pengetahuan tanpa tindakan adalah sia-sia. - Abu Bakar' },
  { genre: 'Pendidikan 10', word: 'Mendidik pikiran tanpa mendidik hati bukanlah pendidikan sama sekali.' },

];
export class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      search: '',
      title: motivasi,
    };
  }

  search = () => {
    let data = motivasi;
    data = data.filter(item =>
      item.genre
        .toLocaleLowerCase()
        .includes(this.state.search.toLocaleLowerCase()),
    );
    this.setState({
      title: data,
    });
  };
  render() {
    return (
      <View>
        <StatusBar backgroundColor="#2c3e50" barStyle="light-content" />
        <View style={style.Itembox}>
          <Text style={style.genre}>Motivasi</Text>
        </View>
        <TextInput
          onChangeText={text => this.setState({ search: text })}
          value={this.state.search}
          placeholder="temukan motivasimu"
          onKeyPress={() => this.search()}
          style={style.input}
        />
        <FlatList
          data={this.state.title}
          renderItem={({ item }) => (
            <View style={style.flat}>
              <Text>{item.genre}</Text>
              <Text>{item.word}</Text>
            </View>
          )}
          keyExtractor={item => item.genre}
        />
      </View>
    );
  }
}

export default Home;

const style = StyleSheet.create({
  input: {
    borderWidth: 1,
    borderRadius: 20,
    marginHorizontal: 10,
    marginVertical: 10,
    padding: 10,
    backgroundColor: 'white',
  },
  flat: {
    borderWidth: 1,
    borderRadius: 10,
    padding: 10,
    marginVertical: 10,
    marginHorizontal: 5,
    backgroundColor: 'cyan',
  },
  Itembox: {
    padding: 20,
    backgroundColor: 'grey',
    shadowColor: 'black',
    shadowOpacity: 0.35,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    elevation: 11,
  },
  genre: {
    color: 'white',
    fontSize: 25,
    fontWeight: 'bold',
  },
});